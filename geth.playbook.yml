#!/usr/bin/env ansible-playbook
---
- name: Configure geth node
  hosts: "{{ custom_hosts | default('geth_nodes') }}"
  become: true

  vars:
    golang_new_version: "1.15.5"
    golang_group: go
    golang_user: geth
    golang_parent_dir: /usr/local
    golang_goroot: "{{ golang_parent_dir }}/go"
    golang_gopath: /go
    golang_gocache: "{{ golang_gopath }}/.cache"
    golang_tar: "go{{golang_new_version}}.linux-amd64.tar.gz"
    golang_url: "https://storage.googleapis.com/golang/{{ golang_tar }}"
    golang_repo_geth: github.com/ethereum/go-ethereum
    geth_version: "v1.9.24"

  environment:
    PATH: "{{ ansible_env.PATH }}:{{ golang_goroot }}/bin:{{ golang_gopath }}/bin"
    GOROOT: "{{ golang_goroot }}"
    GOPATH: "{{ golang_gopath }}"
    GOCACHE: "{{ golang_gocache }}"

  tasks:

    - name: Install dependencies
      apt:
        update_cache: yes
        name:
          - acl
          - git
      tags:
        - system

    - name: Check current Go version
      command: bash -c "{{ golang_goroot }}/bin/go version | sed -e 's/go version go//g' | cut -d' ' -f1"
      ignore_errors: yes
      register: golang_version
      changed_when: false
      tags:
        - go
        - go_version

    - name: Go version
      debug:
        msg:
          - "Go version"
          - "current: {{ golang_version.stdout }}"
          - "new: {{ golang_new_version }}"
      tags:
        - go
        - go_version

    - name: "Create {{ golang_group }} group"
      group:
        name: "{{ golang_group }}"
        state: present
      tags:
        - system
        - go

    - name: "Create {{ golang_user }} user"
      user:
        state: present
        name: "{{ golang_user }}"
        create_home: no
        system: yes
        password: "*"
        shell: /usr/sbin/nologin
        groups: "{{ golang_group }}"
      tags:
        - system
        - go

    - name: "Ensure GOPATH {{ golang_gopath }}"
      file:
        path: "{{ golang_gopath }}"
        state: directory
        owner: "{{ golang_user }}"
        group: "{{ golang_group }}"
        mode: u=rwX,g=rwX,o=rX
        recurse: yes
      tags:
        - system
        - go

    - name: "Ensure {{ golang_gopath }}/pkg"
      file:
        path: "{{ golang_gopath }}/pkg"
        state: directory
        owner: "{{ golang_user }}"
        group: "{{ golang_group }}"
        mode: u=rwX,g=rwX,o=rX
        recurse: yes
      tags:
        - system
        - go

    - name: Check if the Go tar exists
      register: golang_downloaded
      stat:
        path: "/tmp/{{ golang_tar }}"
      tags:
        - go

    - name: Download Go tar
      when: not golang_downloaded.stat.exists
      get_url:
        url: "{{ golang_url }}"
        dest: /tmp
        mode: 0440
      tags:
        - go

    - name: Remove old installation of Go if it is a different version
      when: (go_version.stdout is defined) and (go_version.stdout != golang_new_version )
      file:
        path: "{{ golang_goroot }}"
        state: absent
      tags:
        - go

    - name: Extract the Go tar
      unarchive:
        src: "/tmp/{{ golang_tar }}"
        dest: "{{ golang_parent_dir }}"
        remote_src: yes
        creates: "{{ golang_parent_dir }}/go"
      tags:
        - go

    - name: Set system-wide Go env vars
      copy:
        owner: root
        group: root
        dest: /etc/profile.d/golang.sh
        content: |
          export GOROOT={{ golang_goroot }}
          export GOPATH={{ golang_gopath }}
          export GOCACHE="{{ golang_gocache }}"
          export PATH="$PATH:$GOROOT/bin:$GOPATH/bin"
      tags:
        - system
        - go

    - name: Download geth
      command: go get -d {{ golang_repo_geth }}@{{ geth_version }}
      become: yes
      become_user: "{{ golang_user }}"
      environment:
        GO111MODULE: "on"
      args:
        chdir: "{{ golang_gopath }}/"
        creates: "{{ golang_gopath }}/pkg/mod/{{ golang_repo_geth }}@{{ geth_version }}"
      tags:
        - geth

    - name: Install geth
      command: go install ./...
      become: yes
      become_user: "{{ golang_user }}"
      args:
        chdir: "{{ golang_gopath }}/pkg/mod/{{ golang_repo_geth }}@{{ geth_version }}"
        creates: "{{ golang_gopath }}/bin/geth"
      tags:
        - geth

    # - name: Set /lib/systemd/system/geth.service
    #   copy:
    #     owner: root
    #     group: root
    #     mode: "0644"
    #     dest: /lib/systemd/system/geth.service
    #     content: |
    #       [Unit]
    #       Description=Ethereum go client
    #       After=syslog.target network.target

    #       [Service]
    #       User={{ golang_user }}
    #       Group={{ golang_user }}
    #       Environment=GOPATH={{ golang_gopath }}
    #       Type=simple
    #       ExecStart={{ golang_gopath }}/bin/geth --cache 2048 --rpc  --rpcaddr="0.0.0.0" --rpcvhosts=ethjsonrpc.mchange.com --ipcdisable
    #       # ExecStart={{ golang_gopath }}/bin/geth --cache 2048 --rpc  --rpcaddr="127.0.0.1" --rpcvhosts=ethjsonrpc.mchange.com --ipcdisable
    #       KillMode=process
    #       KillSignal=SIGINT
    #       TimeoutStopSec=90
    #       Restart=on-failure
    #       RestartSec=10s

    #       [Install]
    #       WantedBy=multi-user.target
