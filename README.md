# ansible-playbooks-cryptocurrencies

Ansible playbooks to configure cryptocurrency nodes:

- geth
- faircoin

## Pre-requisites

Ansible must be installed locally to run the playbook.

For example, to install Ansible on Linux with `apt-get`:

``` bash
sudo apt-get update
sudo apt-get install -y python3 python3-pip
pip3 install ansible
```

## Configure

Create a file named `hosts.ini` at the root of the project following the example in `hosts.example.ini` with the host IP and the path to the private SSH key file used to connect to the hosts.

## Run

**For geth:**

``` bash
ansible-playbook geth.playbook.yml [--ask-become-pass]
```

**For faircoin:**

``` bash
ansible-playbook faircoin.playbook.yml [--ask-become-pass]
```

**Notes:**

Add the `--ask-become-pass` flag if the `ansible_user` on the host uses a `sudo` password.

To run the playbooks on the localhost, add the flags:

`--connection=local --extra_vars "custom_hosts=localhost"`

e.g.

``` bash
ansible-playbook geth.playbook.yml --connection=local --extra_vars "custom_hosts=localhost"
```

## Author

**Andre Silva** - [andreswebs](https://gitlab.com/andreswebs)
